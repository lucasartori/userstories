<?php

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');
require_once 'util.php';
require_once 'story.php';
require_once 'pdfUtil.php';


$limit = isset($_GET['limit']) ? (int)$_GET['limit'] : 0;
$offset = isset($_GET['offset']) ? (int)$_GET['offset'] : 0;
$printOffset = isset($_GET['printOffset']) ? (int)$_GET['printOffset'] : 0;
$stories = isset($_GET['stories']) ? json_decode($_GET['stories']) : [];



$result = getCURLDataFromPresetFilter();
$strDatas = json_decode($result);
$issues = convertStories($strDatas->issues);

$issues = filterIssues ($issues, $stories, $limit, $offset);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Luca Sartori');
$pdf->SetTitle('Story Cards');
$pdf->SetSubject('Scrum Story cards generated from selected Jira stories');
$pdf->SetKeywords('TCPDF, PDF, story, cards, jira');

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);



// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

$html = <<<EOF
$css
<table>

EOF;

$index = 1;
for ($i=0;$i<$printOffset;$i++){
  $printStory = '';
     if (($index % 2) == 1){
      $html .= <<<EOF
  <tr>
    <td class="storyCard">$printStory</td>
    <td class="centerColumn"></td>  
EOF;
      
     }
else {
      $html .= <<<EOF
    <td class="storyCard">$printStory</td>
  </tr>
EOF;
      
     }
    $index++;
}

foreach ($issues as $story){
  $printStory = printStoryPDF($story);
     if (($index % 2) == 1){
      $html .= <<<EOF
  <tr>
    <td class="storyCard">$printStory</td>
    <td class="centerColumn"></td>  
EOF;
      
     }
else {
      $html .= <<<EOF
    <td class="storyCard">$printStory</td>
  </tr>
EOF;
      
     }
    $index++;
}
if (($index % 2) == 0){
 $html .= <<<EOF
    <td class="storyCard"></td>
  </tr>
EOF;
}

$html .= <<<EOF
</table>

EOF;

// output the HTML content
$pdf->writeHTML($html, false, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('storyCards.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+