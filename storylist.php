<!DOCTYPE html>
<html>
<head>
	<title>Bug List for Webdev</title>
  
  <style>
    .w3-table-all th{background-color:#4DBF1C; color:white}
    .w3-table-all tr:nth-child(odd){background-color:#fff}
    .w3-table-all tr:nth-child(even){background-color:#f1f1f1}
    .w3-hoverable tbody tr:hover{background-color:#ccc}
    .w3-table-all td,.w3-table-all th{padding:6px 8px;display:table-cell;text-align:left;vertical-align:top}
    .w3-table-all th:first-child,.w3-table-all td:first-child{padding-left:16px}
		@media print
		{    
			.no-print, .no-print *
			{
					display: none !important;
			}
		}
		@media screen
		{    
			.no-screen, .no-screen *
			{
				display: none !important;
			}
		}		
		
	#printButton{
    background: #3B6BF3;
    color: white;
    padding: 10px 20px;
    margin: 10px;
    border: 0px;
    border-radius: 10px;
    font-size: 20px;
	}
		
body{
			margin: 3px;
			padding: 0px;

		}
		.story_row{
			width: 100%;
		}
		.story_card{
			width: 345px;
			height: 245px;
			display: inline-block;
			border: 1px solid black;
			margin: 3px 3px 4px;
			position: relative;
			float: left;
		}
		.story_card.first_row{
			margin-top:8px;
		}
		.story_card_top{
			border-bottom: 1px solid black;
			height: 25px;
			padding: 4px 5px 6px;
			font-size: 20px;
			font-family: Verdana;
		}
		.story_id{
			float: left;
			text-align: left;
			color: #254CBB;
		}
		.story_points{
			float: right;
			text-align: right;
			color: #254CBB;
		}
		.story_card_body{
			height: 155px;
			padding: 5px;
			font-size: 25px;
		}
		.story_card_bottom {
		    /* border-top: 1px solid black; */
		    /* height: 25px; */
		    padding: 4px 5px 6px;
		    font-size: 20px;
		    font-family: Verdana;
		    position: absolute;
		    bottom: 5px;
		    width: 245px;
		    border-radius: 5px;
		    left: 5px;
		}
		.story_epic{
			float: left;
			text-align: left;
		}
		.parent_generic{
			background: #cccccc;
		}
		.epic_generic{
			background: #f79232;
			color: white;
		}
		.story_qrcode {
		    position: absolute;
		    bottom: 1px;
		    right: 10px;
		    overflow: hidden;
		}		
  </style>
	<script>
	function printStories(){
		document.getElementById("printArea").innerHTML = ""
		stories = document.getElementsByClassName('story');
		var story = [];
		numberOfStories = 0;
		for(i=0;i<stories.length;i++){
			if (stories[i].children[0].children[0].checked){
				story = [];
				story.key = stories[i].children[1].textContent;
				story.type = stories[i].children[2].textContent;
				story.parent = stories[i].children[3].textContent;
				story.epic = stories[i].children[4].textContent;
				story.summary = stories[i].children[5].textContent;
				story.points = stories[i].children[6].textContent;
				story.status = stories[i].children[7].textContent;
				printStory(story, numberOfStories);
				numberOfStories++;
			}
		}
		if (numberOfStories>0){
			setTimeout(2000);
			print();
		}
	}

		
	function printStory(story, position){
		var storyNode = document.createElement("div");
		storyNode.className = "story_card";
		if (position>7 && (position%8) <2 ){
			storyNode.className += " first_row";
		}
		
		var storyCardTop = document.createElement("div");
		storyCardTop.className = "story_card_top";
		storyNode.appendChild(storyCardTop);   
		var storyCardBody = document.createElement("div");
		storyCardBody.className = "story_card_body";
		storyNode.appendChild(storyCardBody); 
		var storyCardBottom = document.createElement("div");
		storyCardBottom.className = "story_card_bottom";
		if (story.epic!=''){
			storyCardBottom.className += " epic_generic";
			createAppendDiv(storyCardBottom, "story_epic", "Epic: \n" + story.epic)
		}
		if (story.parent!=''){
			storyCardBottom.className += " parent_generic";
			createAppendDiv(storyCardBottom, "story_epic", "Parent: \n" + story.parent)
		}
		storyNode.appendChild(storyCardBottom); 
		var storyQrCode = document.createElement("div");
		storyQrCode.className = "story_qrcode";
		storyNode.appendChild(storyQrCode); 
		
		createAppendDiv(storyCardTop, "story_id", story.key)
		createAppendDiv(storyCardTop, "story_points", story.points)
		createAppendDiv(storyCardBody, "story_title", story.summary)
			
		var qrNode = document.createElement("img");
		qrNode.setAttribute('src', 'https://chart.googleapis.com/chart?chs=60x60&cht=qr&chl=http%3A%2F%2Feisure.atlassian.net%2Fbrowse%2F'+story.key+'&choe=UTF-8&chld=L|0');
		qrNode.setAttribute('title', 'link to Jira');
		storyQrCode.appendChild(qrNode); 
		
		document.getElementById("printArea").appendChild(storyNode);
	}	
		
	function createAppendDiv(parentID, divClass, textContent){
		var tmpNode = document.createElement("div");
		tmpNode.className = divClass;
		var pointText = document.createTextNode(textContent);
		tmpNode.appendChild(pointText); 
		parentID.appendChild(tmpNode); 	
	}	
		
	function toggle(source) {
		checkboxes = document.getElementsByName('storiesCheckbox');
		for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = source.checked;
		}
	}
	function selectRow(row)
	{
    var firstInput = row.getElementsByTagName('input')[0];
    firstInput.checked = !firstInput.checked;
	}
	</script>
</head>
<body>
	<div id="printArea" class="no-screen"></div>
	<div class="no-print">
	
		<button id="printButton"  onclick="printStories();">
			Print selected Stories
		</button>		
	</div>

<?php
function Xml2Array($contents, $get_attributes=1, $priority = 'tag') {
    if(!$contents) return array();

    if(!function_exists('xml_parser_create')) {
        //print "'xml_parser_create()' function not found!";
        return array();
    }

    //Get the XML parser of PHP - PHP must have this module for the parser to work
    $parser = xml_parser_create('');
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xml_values);
    xml_parser_free($parser);

    if(!$xml_values) return;//Hmm...

    //Initializations
    $xml_array = array();
    $parents = array();
    $opened_tags = array();
    $arr = array();

    $current = &$xml_array; //Refference

    //Go through the tags.
    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array
    foreach($xml_values as $data) {
        unset($attributes,$value);//Remove existing values, or there will be trouble

        //This command will extract these variables into the foreach scope
        // tag(string), type(string), level(int), attributes(array).
        extract($data);//We could use the array by itself, but this cooler.

        $result = array();
        $attributes_data = array();

        if(isset($value)) {
            if($priority == 'tag') $result = $value;
            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
        }

        //Set the attributes too.
        if(isset($attributes) and $get_attributes) {
            foreach($attributes as $attr => $val) {
                if($priority == 'tag') $attributes_data[$attr] = $val;
                else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
            }
        }

        //See tag status and do the needed.
        if($type == "open") {//The starting of the tag '<tag>'
            $parent[$level-1] = &$current;
            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                $current[$tag] = $result;
                if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
                $repeated_tag_index[$tag.'_'.$level] = 1;

                $current = &$current[$tag];

            } else { //There was another element with the same tag name

                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                    $repeated_tag_index[$tag.'_'.$level]++;
                } else {//This section will make the value an array if multiple tags with the same name appear together
                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                    $repeated_tag_index[$tag.'_'.$level] = 2;

                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                        $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                        unset($current[$tag.'_attr']);
                    }

                }
                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
                $current = &$current[$tag][$last_item_index];
            }

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
            //See if the key is already taken.
            if(!isset($current[$tag])) { //New Key
                $current[$tag] = $result;
                $repeated_tag_index[$tag.'_'.$level] = 1;
                if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;


            } else { //If taken, put all things inside a list(array)
                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                    // ...push the new element into that array.
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

                    if($priority == 'tag' and $get_attributes and $attributes_data) {
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag.'_'.$level]++;

                } else { //If it is not an array...
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if($priority == 'tag' and $get_attributes) {
                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }

                        if($attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                        }
                    }
                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
                }
            }

        } elseif($type == 'close') { //End of tag '</tag>'
            $current = &$parent[$level-1];
        }
    }

    return($xml_array);
}

/* run mozilla agent */
$agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1';
$url = 'https://leisure.atlassian.net/sr/jira.issueviews:searchrequest-xml/15300/SearchRequest-14400.xml?tempMax=1000&os_username=belvilla-all&os_password=he3g4RVkykaK';

$ch = curl_init();
curl_setopt($ch, CURLOPT_USERAGENT, $agent); //make it act decent
curl_setopt($ch, CURLOPT_URL, $url);         //set the $url to where your request goes
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //set this flag for results to the variable
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //This is required for HTTPS certs if
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //you don't have some key/password action

/* execute the request */
$result = curl_exec($ch);
curl_close($ch);

$strDatas = Xml2Array($result);

echo '<table id="storyTable" class="w3-table-all no-print"><tr>';
echo "<th><input type='checkbox' name='storiesCheckbox' onClick='toggle(this)'></th>";
echo '<th>Key</th>';
echo '<th>Type</th>';
echo '<th>Parent</th>';
echo '<th>Epic</th>';
echo '<th>Summary</th>';
echo '<th>Story Points</th>';
echo '<th>Status</th>';
echo '</tr>';
foreach ($strDatas['rss']['channel']['item'] as $story) {
	try{
		$printStory['parent'] = '';
		$printStory['points'] = '';
		$printStory['epic'] = '';
		$printStory['key'] = $story['key'];
		$printStory['type'] = $story['type'];
		if (array_key_exists ('parent', $story)){
			$printStory['parent'] = $story['parent'];
		}
		$printStory['summary'] = $story['summary'];
		$printStory['status'] = $story['status'];
		
		foreach ($story['customfields']['customfield'] as $customfield){
			if (array_key_exists ('customfieldname', $customfield)){
				if ( "Epic Link" == $customfield['customfieldname'] ){
					$printStory['epic'] = $customfield['customfieldvalues']['customfieldvalue'];
				}
				if ( "Story Points" == $customfield['customfieldname'] ){
					print_r($customfield);
					$printStory['points'] = $customfield['customfieldvalues']['customfieldvalue'];

				}
			}
		}	
		if ($printStory['points'] == '' || $printStory['points']== '0'){
			$printStory['points'] = 0.5;
		}
	
		echo "<tr class='story' id='".$printStory['key']."' onclick='selectRow(this)'>";
		echo "<td class='select'><input type='checkbox' name='storiesCheckbox' value='".$printStory['key']."'></td>";
		echo "<td class='key'>".$printStory['key']."</td>";
		echo "<td class='type'>".$printStory['type']."</td>";
		echo "<td class='parent'>".$printStory['parent']."</td>";
		echo "<td class='epic'>".$printStory['epic']."</td>";
		echo "<td class='summary'>".$printStory['summary']."</td>";
		echo "<td class='points'>".$printStory['points']."</td>";
		echo "<td class='status'>".$printStory['status']."</td>";
		echo "</tr>";
	} catch (Exception $e) {
	}
}
echo "</table></body></html>";
?>

