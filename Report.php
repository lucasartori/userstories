<?php
class Report {

  public $key = '';
  public $summary = '';
  public $report = '';
  public $points = '';
  public $screenshot = '';
  public $sprint = '';
  public $sprintN = '';
  public $sprintStartDate = '';
  public $sprintEndDate = '';
  
  public function __construct ($jsonStory){
    $this->key = $jsonStory->key;
    $this->summary = $jsonStory->fields->summary;

    $this->report = $jsonStory->fields->customfield_11302;
    $this->points = $jsonStory->fields->customfield_10004;    
    $this->screenshot = $jsonStory->fields->customfield_11303;
    $this->sprint = $jsonStory->fields->customfield_10007;
    $preSprint = strpos ($jsonStory->fields->customfield_10007[0],"[");
    $tmpSprint = substr ($jsonStory->fields->customfield_10007[0], $preSprint);
    $tmpSprint = substr ($tmpSprint,1,strlen($tmpSprint)-2 );
    $tmpSprintArray = explode(",",$tmpSprint);
    
    
  }

}