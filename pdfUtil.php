<?php

function filterIssues ($issues, $stories, $limit){
  $filteredIssues = [];
  if( empty( $stories ) ){
    $filteredIssues=$issues;
  }else{
    foreach ($issues as $story) {
      if (in_array($story->key, $stories)) {
        $filteredIssues[]=$story;
      }
    }
  }
  if ($limit>0){
    $filteredIssues = array_slice ( $filteredIssues , 0, $limit);
  }
  //
  //print_r($filteredIssues);
  return $filteredIssues;
}

function printStoryPDF($story){

$storyID = $story->key;
$storyPoints = $story->points;
$timeEstimate = $story->timeEstimate;
$developers = $story->developers;
$storyTitle = $story->summary;
$storyEpic = $story->epic;
$storyParent= $story->parentStory;
if ($storyEpic){
  $storyEpic = "Epic: ".$story->epic;
}elseif ($storyParent){
  $storyEpic = "Parent: ".$story->parentStory;
}
  
  
$storyCard =  <<<EOF
<table "width=100%">
    <tr class="story_card_top">
      <td class="top_margin" colspan="3"></td>
    </tr>
    <tr class="story_card_top">
      <td class="left_margin"></td>
      <td class="story_id">$storyID</td>
      <td class="story_points">Days: $timeEstimate Dev: $developers</td>
    </tr>
    <tr class="">
      <td class="medium_margin" colspan="3"></td>
    </tr>
    <tr class="story_card_body" >
      <td class="left_margin"></td>
      <td class="story_title" colspan=2>$storyTitle</td>
    </tr>
    <tr class="story_card_bottom">
      <td class="left_margin"></td>
      <td class="story_epic">$storyEpic</td>
      <td class="story_qrcode"><img src="https://chart.googleapis.com/chart?chs=60x60&amp;cht=qr&amp;chl=http%3A%2F%2Feisure.atlassian.net%2Fbrowse%2F$storyID&amp;choe=UTF-8&amp;chld=L|0" title="link to Jira"></td>
    </tr>    
  </table>
EOF;
  return $storyCard;
}

// define some HTML content with style
$css = <<<EOF
<style>
  table{
    padding:0;
    margin: 0;
  }
    td.centerColumn {
    width: 7.1pt;
  }
  td.storyCard {
    width: 280.7pt;
    height:191.7pt;
  }
  .top_margin{
    height: 10pt;
    width: 280.7pt;
    font-size: 1pt;   
  } 
  .medium_margin{
    height: 3pt;
    width: 280.7pt;
    font-size: 1pt;   
  }   
  .left_margin{
    width: 10pt;  
  }
  .story_id{
    height: 25pt;
    width: 130.7pt;
    margin:10pt;
    vertical-align:middle;
    font-size: 16pt;
    color: navy;
    border-bottom:1pt thin black;
  }
   .story_points {
    height: 25pt;
    width: 130pt;
    text-align: right;
    vertical-align: middle;
    font-size: 14pt;    
    border-bottom:1pt thin black;
  }
  .story_card_top{
  }
  .story_title{
    height: 95pt;  
    width: 260.7pt;
    font-size: 18pt;
  }
  .story_epic{
    width: 200.7pt;
    font-size: 16pt;    
  }
  .story_qrcode{
    width: 60pt;
    text-align: right;
    vertical-align:bottom;
  }

</style>
EOF;

