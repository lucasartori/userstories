<?php
function printGoogleChart($issues){

echo <<<EOF	
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('boolean', 'Select');
				data.addColumn('string', 'Key');
        data.addColumn('string', 'Type');
				data.addColumn('string', 'Parent');
        data.addColumn('string', 'Epic');
				data.addColumn('string', 'Summary');
        data.addColumn('number', 'Story Points');
				data.addColumn('string', 'Status');

				
data.addRows([
EOF
	
  foreach ($issues as $story) {
		echo "[false, '".$story->key."','".$story->type."','".$story->parentStory."','".$story->epic."','".addslashes($story->summary)."','".$story->points."','".$story->status."'],";
  }

echo <<<EOF
]);	
			var table = new google.visualization.Table(document.getElementById('table_div'));
    	table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
  	}
	</script>
EOF
  
}