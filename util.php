<?php
require_once 'story.php';
require_once 'Report.php';
require_once 'Sprint.php';

function getCURLDataForReport(){
  $url = "https://leisure.atlassian.net/rest/api/2/search";

	$headers = array(
    'Accept: application/json',
    'Content-Type: application/json',
    'Authorization: Basic YmVsdmlsbGEtYWxsOmhlM2c0UlZreWthSw=='
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
  curl_setopt($ch,CURLOPT_POSTFIELDS, '{"jql":"project=WES AND sprint = 112 AND Report = Yes"}');
  curl_setopt($ch, CURLOPT_URL, $url);

  /* execute the request */
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}

function getCURLDataFromPresetFilter(){
  $url = "https://leisure.atlassian.net/rest/api/2/search";

	$headers = array(
    'Accept: application/json',
    'Content-Type: application/json',
    'Authorization: Basic YmVsdmlsbGEtYWxsOmhlM2c0UlZreWthSw=='
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
  curl_setopt($ch,CURLOPT_POSTFIELDS, '{"jql":"project=WES AND issuetype = Story and sprint in openSprints() ORDER BY created DESC"}');
  curl_setopt($ch, CURLOPT_URL, $url);

  /* execute the request */
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}

function convertReports($jsonIssues){
  $issues =[];
  foreach ($jsonIssues as $story) {
    array_push ($issues, new Report($story));
  }
  return $issues;
}

function printReport($issues){
//   foreach ($issues as $story) {
// 		echo "<pre>";
// 		print_r($story);
// 		print_r($story->sprint);
// 		echo "</pre><br/><br/>";
// 	}
	$sprint = new Sprint($issues);
	echo "Report for ".$sprint->name." <br/>";
	echo "Started on ".$sprint->startDate." <br/>";
	echo "Ended on ".$sprint->endDate." <br/>";
  echo '<table id="storyTable" class="w3-table-all no-print"><tr>';
//   echo '<th>Key</th>';
//   echo '<th>Summary</th>';
//   echo '<th>Report</th>';
//   echo '<th>Screenshot</th>';
//   echo '</tr>';
  foreach ($issues as $story) {
    printSingleReport($story);
  }
  echo "</table>";
}


function printSingleReport($story){
 try{
	
		echo "<tr>";
			echo "<td class='key'>".$story->key." - ".$story->summary."</td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td class='summary'>".$story->report."</td>";
		echo "</tr>";	 
 		echo "<tr>";
			echo "<td class='screenshot'><img src='".$story->screenshot."'/></td>";
		echo "</tr>";	 


	} catch (Exception $e) {
	}
}

function getSprintNumber($issues){
	$sprint = [];
	
	$story = end($issues);
	$sprintData = end($story->sprint);	
		
		$preSprint = strpos ($sprintData,"[");
    $tmpSprint = substr ($sprintData, $preSprint);
    $tmpSprint = substr ($tmpSprint,1,strlen($tmpSprint)-2 );
    $tmpSprintArray = explode(",",$tmpSprint);
		foreach ($tmpSprintArray as $val){
			$couple = explode("=",$val);
			$sprint[$couple[0]]=$couple[1];
		}
    

  
	return $sprint["name"];
}
function printTable($issues){
  echo '<table id="storyTable" class="w3-table-all no-print"><tr>';
  echo "<th class='select_header'><input type='checkbox' name='storiesCheckbox' onClick='toggle(this)'></th>";
  echo '<th>Key</th>';
//   echo '<th>Type</th>';
//   echo '<th>Parent</th>';
  echo '<th>Epic</th>';
  echo '<th>Summary</th>';
  echo '<th>Estimate</th>';
  echo '<th>Developers</th>';
  echo '<th>Status</th>';
  echo '</tr>';
  foreach ($issues as $story) {
    printStory($story);
  }
  echo "</table>";
}

function convertStories($jsonIssues){
  $issues =[];
  foreach ($jsonIssues as $story) {
    array_push ($issues, new Story($story));
  }
  return $issues;
}

function printStory($story){
 try{
	
		echo "<tr class='story' id='".$story->key."' onclick='selectRow(this)'>";
		echo "<td class='select'><input type='checkbox' name='storiesCheckbox' value='".$story->key."'></td>";
		echo "<td class='key'>".$story->key."</td>";
// 		echo "<td class='type'>".$story->type."</td>";
// 		echo "<td class='parent'>";
//       echo $story->parentStory;
//     echo "</td>";
		echo "<td class='epic'>".$story->epic."</td>";
		echo "<td class='summary'>".$story->summary."</td>";
		echo "<td class='points'>".$story->timeEstimate."</td>";
		echo "<td class='dev'>".$story->developers."</td>";
		echo "<td class='status'>".$story->status."</td>";
		echo "</tr>";
	} catch (Exception $e) {
	}
}

function printCards($issues){
  echo '<div id="cardsContainer" class="no-screen">';
  foreach ($issues as $story) {
    printCard($story);
  }
  echo "</div>";
}
function printCard($story){
  echo '<div id="card_'.$story->key.'" class="story_card hidden">';
    echo '<div class="story_card_top">';
      echo '<div class="story_id">'.$story->key.'</div>';
      echo '<div class="story_points">'.$story->timeEstimate.'</div>';
    echo '</div>';
    echo '<div class="story_card_body">';
      echo '<div class="story_title">'.$story->summary.'</div>';
    echo '</div>';
    if ($story->epic!=''){
      echo '<div class="story_card_bottom epic_generic">';
        echo '<div class="story_epic">Epic: '.$story->epic.'</div>';
      echo '</div>';
    } elseif ($story->parentStory!=''){
      echo '<div class="story_card_bottom parent_generic">';
        echo '<div class="story_epic">Parent: '.$story->parentStory.'</div>';
      echo '</div>';      
    } else{
      echo '<div class="story_card_bottom"></div>';
    }
    echo '<div class="story_qrcode">';
      echo '<img src="https://chart.googleapis.com/chart?chs=60x60&amp;cht=qr&amp;chl=http%3A%2F%2Feisure.atlassian.net%2Fbrowse%2F'.$story->key.'&amp;choe=UTF-8&amp;chld=L|0" title="link to Jira">';
    echo '</div>';
  echo '</div>';
}


function printGoogleChart($issues){

echo <<<EOF
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('boolean', 'Select');
				data.addColumn('string', 'Key');
        data.addColumn('string', 'Type');
				data.addColumn('string', 'Parent');
        data.addColumn('string', 'Epic');
				data.addColumn('string', 'Summary');
        data.addColumn('number', 'Story Points');
				data.addColumn('string', 'Status');

				
data.addRows([
EOF;
	
  foreach ($issues as $story) {
		echo "[false, '".$story->key."','".$story->type."','".$story->parentStory."','".$story->epic."','".addslashes($story->summary)."','".$story->points."','".$story->status."'],";
  }

echo <<<EOF
]);	
			var table = new google.visualization.Table(document.getElementById('table_div'));
    	table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
  	}
	</script>
EOF;
  
}