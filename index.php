<?php
require_once 'util.php';
require_once 'story.php';

$result = getCURLDataFromPresetFilter();
$strDatas = json_decode($result);
// echo "<pre>";
// print_r($strDatas->issues);
// echo "</pre>";
$issues = convertStories($strDatas->issues);

?>	
<!DOCTYPE html>
<html>
<head>
	<title>Bug List for Webdev</title>
  	<link rel="stylesheet" type="text/css" href="css/style.css">

		<script>
		
	function printStories(){
		document.getElementById("printArea").innerHTML = ""
		stories = document.getElementsByClassName('story');
		var story = [];
		numberOfStories = 0;
		storyCards = document.getElementById('cardsContainer');
		for(i=0;i<storyCards.children.length;i++){
			storyCards.children[i].className = "story_card hidden";
		}
		for(i=0;i<stories.length;i++){
			if (stories[i].children[0].children[0].checked){
				storyCard = document.getElementById('card_'+ stories[i].children[0].children[0].value);
				storyCard.className = "story_card";				
				numberOfStories++;
				story.push(stories[i].children[0].children[0].value);
			}
		}
		if (numberOfStories>0){
			document.getElementById("printArea").innerHTML =JSON.stringify(story);
			printOffset= document.getElementById("printOffset").value
			window.open('storyPdf.php?stories='+JSON.stringify(story)+'&printOffset='+printOffset);
			//print();
		}
	}
	
	function toggle(source) {
		checkboxes = document.getElementsByName('storiesCheckbox');
		for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = source.checked;
			if (checkboxes[i].checked){
				checkboxes[i].parentElement.parentElement.className ='story selected';
			}else{
				checkboxes[i].parentElement.parentElement.className ='story';
			}	
		}
	}
	function selectRow(row)
	{
    var firstInput = row.getElementsByTagName('input')[0];
		firstInput.checked = !firstInput.checked;
	  if (firstInput.checked){
			row.className ='story selected';
		}else{
			row.className ='story';
		}	
		
		
	}
	</script>

</head>
<body>	
	<div id="printArea" class="no-screen"></div>
	<div class="no-print">
	
		<button id="printButton" onclick="printStories();">
			Print selected Stories
		</button>		
		Print Offset
		<select id="printOffset" name="printOffset">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
		</select>
	</div>
	<?php
// print "<pre>";
// print_r($issues);
// print "</pre>";
printCards($issues);
printTable($issues);
?>
<div id="table_div" class="no-print"></div>
</body>
</html>


