<?php
class Sprint {
  
  public $id = "";
  public $state = "";
  public $name = "";
  public $startDate = "";
  public $endDate = "";
  
  
  public function __construct ($issues){
    $story = end($issues);
    $sprintData = end($story->sprint);	
		
		$preSprint = strpos ($sprintData,"[");
    $tmpSprint = substr ($sprintData, $preSprint);
    $tmpSprint = substr ($tmpSprint,1,strlen($tmpSprint)-2 );
    $tmpSprintArray = explode(",",$tmpSprint);
		foreach ($tmpSprintArray as $val){
			$couple = explode("=",$val);
			$sprint[$couple[0]]=$couple[1];
		}
    $this->id = $sprint["id"];
    $this->state = $sprint["state"];
    $this->name = $sprint["name"];
    $startDate = new DateTime($sprint["startDate"]);
    $this->startDate = $startDate->format('d/m/Y');
    $endDate = new DateTime($sprint["endDate"]);
    $this->endDate = $endDate->format('d/m/Y');
    
    
  }
  
}