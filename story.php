<?php
class Story {

  public $key = '';
  public $summary = '';
  public $type = '';
  public $parentStory = '';
  public $epic = '';
  public $points = '';
  public $status = '';
  public $timeEstimate = '';
  public $developers = '';
  
  public function __construct ($jsonStory){
    $this->key = $jsonStory->key;
    $this->summary = $jsonStory->fields->summary;
    $this->type = $jsonStory->fields->issuetype->name;
    if (property_exists ($jsonStory->fields, 'parent')){
     $this->parentStory =  $jsonStory->fields->parent->key;
    }
    $this->timeEstimate = round($jsonStory->fields->timeestimate/28800,1);
    $this->epic = $jsonStory->fields->customfield_10008;
    $this->points = $jsonStory->fields->customfield_10004;
    $this->status = $jsonStory->fields->status->name;
    $this->developers = $jsonStory->fields->customfield_11000;
    if ($this->developers==""){
      $this->developers=1;
    }
  }

}